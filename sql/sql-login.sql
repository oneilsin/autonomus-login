use master
go
-- Nota... primero crea la base de datos, despu�s ejecuta los codigos para crear la tabla y los procedimientos.
--  create database Storage
go
use master
go
use Storage
go

--- TABLA
if(OBJECT_ID('Usuario')is not null)
begin drop table Usuario end
go
create table Usuario(
	Id int identity not null
	, Nombre varchar(50)
	, Apellido varchar(50)
	, Email varchar(50)
	, Telefono varchar(20)
	, UserName varchar(20)
	, Password varchar(max) -- Sin encriptar
	, primary key(Id)
)
insert into Usuario values('Napoleon','Bonaparte','nbonaparte@mail.com','+51 999 999 999','Napoleon','123456789')
go

--- VALIDACI�N -> Usuario
if(OBJECT_ID('SpUsuarioLoginUser')is not null)
begin drop procedure SpUsuarioLoginUser end
go
create procedure SpUsuarioLoginUser(
	@UserName varchar(20)
)
as begin
	select count(1) as Value from Usuario where UserName=@UserName
end
go

--- VALIDACI�N -> Contrase�a
if(OBJECT_ID('SpUsuarioLoginPassword')is not null)
begin drop procedure SpUsuarioLoginPassword end
go
create procedure SpUsuarioLoginPassword(
	@UserName varchar(20)
	, @Password varchar(max)
)
as begin
	select *
	from Usuario 
	where UserName=@UserName
	and Password=@Password
end
go


exec SpUsuarioLoginUser 'napoleon'
exec SpUsuarioLoginPassword 'napoleon','123456789'

