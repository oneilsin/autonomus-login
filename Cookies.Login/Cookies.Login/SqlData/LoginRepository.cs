﻿using Cookies.Login.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cookies.Login.SqlData
{
    public class LoginRepository
    {
        // Cadena de conexón al SQL
        static string sql = "Data Source=NATHAN;Initial Catalog=Storage;Integrated Security=True";

        // Metodo estático, para no instanciar la clase, de validación de datos
        public static bool GetUserIdentification(string userName)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(sql))
                {
                    cn.Open();
                    var command = new SqlCommand("SpUsuarioLoginUser", cn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@UserName", userName));

                    var result = command.ExecuteScalar();
                    return int.Parse(result.ToString()) > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {  throw ex; }
        }
        public static Usuario GetFinalLogin(string userName, string password)
        {
            try
            {
                var user = new Usuario();
                using (SqlConnection cn = new SqlConnection(sql))
                {
                    cn.Open();
                    var da = new SqlDataAdapter("SpUsuarioLoginPassword", cn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@UserName", userName));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Password", password));
                    var reader = da.SelectCommand.ExecuteReader();
                    while (reader.Read())
                    {
                        user.Id = int.Parse(reader["Id"].ToString());
                        user.Nombre = reader["Nombre"].ToString();
                        user.Apellido = reader["Apellido"].ToString();
                        user.Email = reader["Email"].ToString();
                        user.Telefono = reader["Telefono"].ToString();
                        user.UserName = reader["UserName"].ToString();
                        user.Password = reader["Password"].ToString();
                    }

                    return user;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
    }
}
