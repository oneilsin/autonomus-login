﻿using Cookies.Login.SqlData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cookies.Login.Client
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        /*  #    ¿Qué vámos a implementa?                         Estado
         *  ---------------------------------------------------------------
            0.   Crear Tabla y Procedimientos                     P
            1.   Conexión a la base de datos SQL                  P
            2.   Crear Clases (Usuario, Cookies)                  P
            3.   Comandos para Validar Datos                      P
            4.   Crear formulario Login y Menu                    P
            5.   Crear Metodo para validar Usuario                P
            6.   Crear Método para validar Contraseña             P
            7.   Crear Método registra usuario en Cookies         P
            8.   Testing                                          P
        */

        private void ValidarUsuario(string usuario)
        {
            var result = LoginRepository.GetUserIdentification(usuario);
            if (result)
            {
                txPassword.Enabled = true;
                btnIniciar.Enabled = true;
                txUsuario.Error = "";
                txPassword.Focus();
            }
            else
            {
                txPassword.Enabled = false;
                btnIniciar.Enabled = false;
                txUsuario.Error = "Usuanio no Existe";
            }
        }
        private void ValidarContraseña(string usuario, string contraseña)
        {
            var user = LoginRepository.GetFinalLogin(usuario,contraseña);
            if (user != null)
            {
                //Registrar el Cookies
                Program.Cookie = new Model.Cookies();
                Program.Cookie.Id = user.Id;
                Program.Cookie.Nombre = user.Nombre;
                Program.Cookie.Apellido = user.Apellido;
                Program.Cookie.Email = user.Email;
                Program.Cookie.Telefono = user.Telefono;
                Program.Cookie.Version = "version 00.001";
                Program.Cookie.IGV = 0.18m;

                //Abrir el menu

                var f = new frmMenu();
                this.Hide();
                f.ShowDialog();

                txPassword.Error = "";
            }
            else
            {
                txPassword.Error = "Contraseña o usuario incorrecto";
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            ValidarContraseña(txUsuario.Text, txPassword.Text);
        }

        private void txUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ValidarUsuario(txUsuario.Text);
        }

        private void txPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ValidarContraseña(txUsuario.Text, txPassword.Text);
        }
    }
}
