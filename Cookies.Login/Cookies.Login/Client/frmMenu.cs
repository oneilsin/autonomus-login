﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cookies.Login.Client
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;

            lbDescription.Text = $"usuario: {(string.Concat(Program.Cookie.Apellido, ", ", Program.Cookie.Nombre))}" +
                $", Email:{Program.Cookie.Email}, Telefono:{Program.Cookie.Telefono}, {Program.Cookie.Version}";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var f = new frmDatos();
            f.ShowDialog();
        }
    }
}
