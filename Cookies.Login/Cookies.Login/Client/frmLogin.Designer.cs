﻿
namespace Cookies.Login.Client
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.txUsuario = new Autonomo.Control.FlatTextBox();
            this.txPassword = new Autonomo.Control.FlatTextBox();
            this.btnIniciar = new Autonomo.Control.CustomButton();
            this.SuspendLayout();
            // 
            // txUsuario
            // 
            this.txUsuario.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txUsuario.BackColor = System.Drawing.Color.White;
            this.txUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txUsuario.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txUsuario.ColorLine = System.Drawing.Color.MediumSeaGreen;
            this.txUsuario.ColorText = System.Drawing.SystemColors.WindowText;
            this.txUsuario.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txUsuario.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txUsuario.Error = "";
            this.txUsuario.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txUsuario.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txUsuario.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txUsuario.ImageIcon")));
            this.txUsuario.Info = "";
            this.txUsuario.Location = new System.Drawing.Point(49, 97);
            this.txUsuario.MaterialStyle = false;
            this.txUsuario.MaxLength = 32767;
            this.txUsuario.MultiLineText = false;
            this.txUsuario.Name = "txUsuario";
            this.txUsuario.PasswordChar = '\0';
            this.txUsuario.Placeholder = "nombre del usuario";
            this.txUsuario.ReadOnly = false;
            this.txUsuario.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txUsuario.Size = new System.Drawing.Size(224, 57);
            this.txUsuario.SizeLine = 2;
            this.txUsuario.TabIndex = 0;
            this.txUsuario.Title = "Identificador del control";
            this.txUsuario.VisibleIcon = true;
            this.txUsuario.VisibleTitle = false;
            this.txUsuario.KeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.txUsuario_KeyDown);
            // 
            // txPassword
            // 
            this.txPassword.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txPassword.BackColor = System.Drawing.Color.White;
            this.txPassword.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txPassword.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txPassword.ColorLine = System.Drawing.Color.MediumSeaGreen;
            this.txPassword.ColorText = System.Drawing.SystemColors.WindowText;
            this.txPassword.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txPassword.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txPassword.Enabled = false;
            this.txPassword.Error = "";
            this.txPassword.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txPassword.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txPassword.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txPassword.ImageIcon")));
            this.txPassword.Info = "";
            this.txPassword.Location = new System.Drawing.Point(49, 161);
            this.txPassword.MaterialStyle = false;
            this.txPassword.MaxLength = 32767;
            this.txPassword.MultiLineText = false;
            this.txPassword.Name = "txPassword";
            this.txPassword.PasswordChar = '\0';
            this.txPassword.Placeholder = "Contraseña";
            this.txPassword.ReadOnly = false;
            this.txPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txPassword.Size = new System.Drawing.Size(224, 57);
            this.txPassword.SizeLine = 2;
            this.txPassword.TabIndex = 1;
            this.txPassword.Title = "Identificador del control";
            this.txPassword.VisibleIcon = true;
            this.txPassword.VisibleTitle = false;
            this.txPassword.KeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.txPassword_KeyDown);
            // 
            // btnIniciar
            // 
            this.btnIniciar.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.btnIniciar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIniciar.Enabled = false;
            this.btnIniciar.FlatAppearance.BorderSize = 0;
            this.btnIniciar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIniciar.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnIniciar.ForeColor = System.Drawing.Color.White;
            this.btnIniciar.Location = new System.Drawing.Point(80, 239);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(165, 29);
            this.btnIniciar.TabIndex = 2;
            this.btnIniciar.Text = "Iniciar Sesión";
            this.btnIniciar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnIniciar.UseVisualStyleBackColor = false;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // frmLogin
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(322, 280);
            this.Controls.Add(this.btnIniciar);
            this.Controls.Add(this.txPassword);
            this.Controls.Add(this.txUsuario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLogin";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.Control.FlatTextBox txUsuario;
        private Autonomo.Control.FlatTextBox txPassword;
        private Autonomo.Control.CustomButton btnIniciar;
    }
}